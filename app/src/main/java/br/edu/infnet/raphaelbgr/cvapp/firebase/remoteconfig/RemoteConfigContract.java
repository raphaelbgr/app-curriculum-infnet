package br.edu.infnet.raphaelbgr.cvapp.firebase.remoteconfig;


interface RemoteConfigContract {
    void onPictureUrlReceived(String updateUrl);

    interface OnRemoteSuccessfullyFetched {
        void processFetchResponse();
    }
}
