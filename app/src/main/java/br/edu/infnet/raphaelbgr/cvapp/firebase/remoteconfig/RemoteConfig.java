package br.edu.infnet.raphaelbgr.cvapp.firebase.remoteconfig;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import br.edu.infnet.raphaelbgr.cvapp.App;
import br.edu.infnet.raphaelbgr.cvapp.R;

public class RemoteConfig {

    private static final String TAG = App.class.getSimpleName();

    private static final int DEFAULT_REMOTE_CONFIG_FETCH_CACHE_SECONDS = 0;
    private static final String KEY_PICTURE_URL = "key_picture_url";
    private RemoteConfigContract cvcRemoteConfigListener;


    private RemoteConfig() {
        throw new IllegalStateException("Class cannot be instantiated without a Listener.");
    }

    RemoteConfig(@NonNull RemoteConfigContract cvcRemoteConfigListener) {
        this.cvcRemoteConfigListener = cvcRemoteConfigListener;
    }

    public static Builder start() {
        return new Builder();
    }

    private void startRemoteConfigService() {
        final FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .build();
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        fetchRemoteParameters(firebaseRemoteConfig, null);
    }

    private void fetchRemoteParameters(FirebaseRemoteConfig firebaseRemoteConfig,
                                       RemoteConfigContract.OnRemoteSuccessfullyFetched onRemoteSuccessfullyFetched) {
        firebaseRemoteConfig.fetch(DEFAULT_REMOTE_CONFIG_FETCH_CACHE_SECONDS)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        firebaseRemoteConfig.activate();
                    } else {
                        Log.w(TAG, "Did not load the remote config informations.");
                    }
                    if (onRemoteSuccessfullyFetched != null) {
                        onRemoteSuccessfullyFetched.processFetchResponse();
                    }
                });
    }

    private void checkForPictureUrl() {
        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();

        fetchRemoteParameters(remoteConfig, () -> {
            if (remoteConfig.getBoolean(KEY_PICTURE_URL)) {
                final String updateUrl = remoteConfig.getString(KEY_PICTURE_URL);

                if (cvcRemoteConfigListener != null) {
                    cvcRemoteConfigListener.onPictureUrlReceived(updateUrl);
                }
            }
        });
    }

    public static class Builder {

        private RemoteConfigContract remoteConfigListener;

        public Builder() {
        }

        public Builder setListener(RemoteConfigContract cvcRemoteConfigListener) {
            this.remoteConfigListener = cvcRemoteConfigListener;
            return this;
        }

        public RemoteConfig build() {
            return new RemoteConfig(remoteConfigListener);
        }

        public RemoteConfig checkForPictUrl() {
            final RemoteConfig remoteConfig = build();
            remoteConfig.checkForPictureUrl();
            return remoteConfig;
        }

        public RemoteConfig startRemoteConfigService() {
            final RemoteConfig remoteConfig = build();
            remoteConfig.startRemoteConfigService();
            return remoteConfig;
        }
    }
}
