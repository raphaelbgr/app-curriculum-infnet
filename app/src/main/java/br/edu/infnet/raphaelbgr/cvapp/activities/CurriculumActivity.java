package br.edu.infnet.raphaelbgr.cvapp.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import br.edu.infnet.raphaelbgr.cvapp.App;
import br.edu.infnet.raphaelbgr.cvapp.R;
import br.edu.infnet.raphaelbgr.cvapp.data.datamodel.SingleEntry;
import br.edu.infnet.raphaelbgr.cvapp.fragments.PersonalInfoFragment;
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback;
import br.edu.infnet.raphaelbgr.cvapp.navigator.AppNavigator;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CurriculumActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CurriculumActivityCallback {

    @BindView(R.id.welcome_screen)
    View welcomeScreen;

    @BindView(R.id.activity_container)
    CoordinatorLayout activityContainer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private ActionBarDrawerToggle toggle;
    private Fragment personalInfoFragment;
    private Fragment professionalExperience;
    private Fragment academicFragment;
    private Fragment publicationsFragment;
    private Fragment coursesFragment;
    private Fragment abilitiesFragment;
    private Fragment previewFragment;
    private CurriculumActivityCallback.FabInteractor fabInteractor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curriculum);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setupListeners();
        setFabVisibility(false);
        populateDrawerData();
    }

    private void populateDrawerData() {
        App.getDao().getObject(PersonalInfoFragment.TAG, SingleEntry.class, info -> {
            SingleEntry data = (SingleEntry) info;
            if (data != null) {
                TextView navName = navigationView.getHeaderView(0).findViewById(R.id.nav_user_name);
                TextView navEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_user_email);
                navName.setText(data.getName());
                navEmail.setText(data.getEmail());
            }
        });

    }

    private void setupListeners() {
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        fab.setOnClickListener(view -> {
            if (fabInteractor != null)
                fabInteractor.onFabEditClicked(view);
        });
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (welcomeScreen.getWindowVisibility() == View.VISIBLE) {
            welcomeScreen.setVisibility(View.GONE);
        }

        Fragment fragment = null;
        if (id == R.id.personal_info) {
            fragment = getPersonalInfoFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.academic) {
            fragment = getAcademicFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.experience) {
            fragment = getProfessionalExperienceFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.courses) {
            fragment = createCoursesFragmentFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.publications) {
            fragment = getPublicationsFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.abilities) {
            fragment = getAbilitiesFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.visualize) {
            fragment = getPreviewFragment();
            fabInteractor = (FabInteractor) fragment;
        } else if (id == R.id.nav_erase) {
            App.getDao().eraseAllData();
            showSnackMessage(getString(R.string.data_erased));
        } else if (id == R.id.nav_exit) {
            finish();
        }

        if (fragment != null) {
            switchFragment(fragment, null);
            fab.setAlpha(1f);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void switchFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    private Fragment getPersonalInfoFragment() {
        if (personalInfoFragment == null) {
            FragmentManager manager = getSupportFragmentManager();
            personalInfoFragment = AppNavigator.createPersonalInfoFragment(manager);
        }
        return personalInfoFragment;
    }

    private Fragment getProfessionalExperienceFragment() {
        if (professionalExperience == null) {
            FragmentManager manager = getSupportFragmentManager();
            professionalExperience = AppNavigator.createProfessionalExperienceFragment(manager);
        }
        return professionalExperience;
    }

    private Fragment getPublicationsFragment() {
        if (publicationsFragment == null) {
            FragmentManager manager = getSupportFragmentManager();
            publicationsFragment = AppNavigator.createPublicationsFragment(manager);
        }
        return publicationsFragment;
    }

    private Fragment getAbilitiesFragment() {
        if (abilitiesFragment == null) {
            FragmentManager manager = getSupportFragmentManager();
            abilitiesFragment = AppNavigator.createAbilitiesFragment(manager);
        }
        return abilitiesFragment;
    }

    private Fragment getPreviewFragment() {
        if (previewFragment == null) {
            FragmentManager manager = getSupportFragmentManager();
            previewFragment = AppNavigator.createPreviewFragment(manager);
        }
        return previewFragment;
    }

    private Fragment createCoursesFragmentFragment() {
        if (coursesFragment == null) {
            FragmentManager manager = getSupportFragmentManager();
            coursesFragment = AppNavigator.createCoursesFragment(manager);
        }
        return coursesFragment;
    }

    private Fragment getAcademicFragment() {
        if (academicFragment == null) {
            FragmentManager manager = getSupportFragmentManager();
            academicFragment = AppNavigator.createAcademicFragment(manager);
        }
        return academicFragment;
    }

    @Override
    public void setSelectedDrawerTab(int id) {
        navigationView.setCheckedItem(id);
    }

    @Override
    public void setFabVisibility(boolean visibility) {
        fab.setVisibility(visibility ? FloatingActionButton.VISIBLE : FloatingActionButton.INVISIBLE);
    }

    @Override
    public void showSnackMessage(String s) {
        Snackbar.make(activityContainer, s, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setTitle(String title) {
        toolbar.setTitle(title);
    }
}
