package br.edu.infnet.raphaelbgr.cvapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.edu.infnet.raphaelbgr.cvapp.R
import br.edu.infnet.raphaelbgr.cvapp.base.BaseFragment
import br.edu.infnet.raphaelbgr.cvapp.databinding.FragmentProfessionalExperienceBinding
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback.FabInteractor

class ProfessionalExperienceFragment : BaseFragment(), FabInteractor {
    private var _binding: FragmentProfessionalExperienceBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentProfessionalExperienceBinding.bind(inflater.inflate(R.layout.fragment_professional_experience, container, false))
        retainInstance = true
        setFieldsStatus(false)
        activityCallback.setTitle(getString(R.string.professional_experience))
        viewForComparison = binding.etLastJobTitle
        onResumeDrawerSelection = R.id.experience
        activityCallback.setFabVisibility(true)
        return binding.root
    }

    override fun populateFields() {
        binding.etLastJobTitle.setText(singleEntry.lastJobTitle)
        binding.etLastSalary.setText(singleEntry.lastSalary)
        binding.etEnterpriseName.setText(singleEntry.enterpriseName)
    }

    override fun readFields() {
        singleEntry.enterpriseName = binding.etEnterpriseName.text.toString()
        singleEntry.lastSalary = binding.etLastSalary.text.toString()
        singleEntry.lastJobTitle = binding.etLastJobTitle.text.toString()
    }

    override fun setFieldsStatus(status: Boolean) {
        binding.etEnterpriseName.isEnabled = status
        binding.etLastSalary.isEnabled = status
        binding.etLastJobTitle.isEnabled = status
    }
}