package br.edu.infnet.raphaelbgr.cvapp.data;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import br.edu.infnet.raphaelbgr.cvapp.data.sqlite.FeedReaderContract;
import br.edu.infnet.raphaelbgr.cvapp.data.tasks.TaskAsyncGetFromDisk;

public class SqliteDao implements Dao {

    private final FeedReaderContract.FeedReaderDbHelper mDbHelper;
    private final String TAG = SqliteDao.class.getSimpleName();
    private final Application application;
    private SQLiteDatabase db;

    private SqliteDao(Application application) {
        this.mDbHelper = new FeedReaderContract.FeedReaderDbHelper(application);
        this.application = application;
        init();
    }

    private void init() {
        // Gets the data repository in write mode
        db = mDbHelper.getWritableDatabase();
    }

    @Override
    public void saveObject(Object object) {
//        ContentValues values = new ContentValues();
//        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE, title);
//        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE, subtitle);
//
//        // Insert the new row, returning the primary key value of the new row
//        long newRowId = db.insert(FeedReaderContract.FeedEntry.TABLE_NAME, null, values);
    }

    @Override
    public void getObject(String key, Class<?> aClass, TaskAsyncGetFromDisk.GetFromDiskCallBack taskAsyncGetFromDisk) {
        Object object = null;
        try {
            SharedPreferences sharedPref = application.getSharedPreferences(TAG, Context.MODE_PRIVATE);
            String json = sharedPref.getString(key, null);
            object = new Gson().fromJson(json, aClass);
        } catch (JsonSyntaxException e) {
            Log.w(TAG, e.getCause());
        }
    }

    @Override
    public void getObject(Class<?> aClass, TaskAsyncGetFromDisk.GetFromDiskCallBack callBack) {

    }

    @Override
    public void eraseAllData() {
        SharedPreferences sharedPref = application.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

    public static final class Builder {
        public static SqliteDao init(Application application) {
            return new SqliteDao(application);
        }
    }
}
