package br.edu.infnet.raphaelbgr.cvapp.firebase.analytics;


import android.content.Context;
import android.os.Bundle;


public class FirebaseAnalytics {

    public final static String SCREEN = "SCREEN";

    public static void logAnalyticsClickEvent(Context context, String category, String label) {
        Bundle bundle = new Bundle();
        bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID, label);
        bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE, category);
        com.google.firebase.analytics.FirebaseAnalytics.getInstance(context).logEvent(com.google.firebase.analytics.FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
