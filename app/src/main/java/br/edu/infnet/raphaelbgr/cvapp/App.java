package br.edu.infnet.raphaelbgr.cvapp;

import android.app.Application;
import android.content.Context;

import br.edu.infnet.raphaelbgr.cvapp.data.Dao;
import br.edu.infnet.raphaelbgr.cvapp.data.SharedPrefsDao;
import br.edu.infnet.raphaelbgr.cvapp.firebase.remoteconfig.RemoteConfig;


public class App extends Application {

    private static Dao dao;
    private static App app;

    public static Context getInstance() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupFirebaseRemoteConfig();
        dao = SharedPrefsDao.Builder.init(this);
        app = this;

    }

    private void setupFirebaseRemoteConfig() {
        RemoteConfig.start().startRemoteConfigService();
    }

    public static Dao getDao() {
        return dao;
    }
}
