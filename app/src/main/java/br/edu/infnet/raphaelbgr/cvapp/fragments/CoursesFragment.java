package br.edu.infnet.raphaelbgr.cvapp.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import br.edu.infnet.raphaelbgr.cvapp.R;
import br.edu.infnet.raphaelbgr.cvapp.base.BaseFragment;
import br.edu.infnet.raphaelbgr.cvapp.databinding.FragmentCoursesBinding;
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback;

public class CoursesFragment extends BaseFragment implements CurriculumActivityCallback.FabInteractor {

    public static final String TAG = CoursesFragment.class.getSimpleName();

    private FragmentCoursesBinding binding = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCoursesBinding.bind(inflater.inflate(R.layout.fragment_courses, container, false));
        setRetainInstance(true);
        setFieldsStatus(false);
        activityCallback.setTitle(getString(R.string.courses));
        viewForComparison = binding.etLastCourseName;
        onResumeDrawerSelection = R.id.courses;
        activityCallback.setFabVisibility(true);
        return binding.getRoot();
    }

    protected void populateFields() {
        binding.etLastCourseName.setText(singleEntry.getLastCourseName());
        binding.etLastCourseInstitutionName.setText(singleEntry.getLastCourseInstitutionName());
        binding.etLastCourseGraduationYear.setText(singleEntry.getLastCourseGraduationYear());
    }

    protected void readFields() {
        singleEntry.setLastCourseName(binding.etLastCourseName.getText().toString());
        singleEntry.setLastCourseInstitutionName(binding.etLastCourseInstitutionName.getText().toString());
        singleEntry.setLastCourseGraduationYear(binding.etLastCourseGraduationYear.getText().toString());
    }

    protected void setFieldsStatus(boolean status) {
        binding.etLastCourseName.setEnabled(status);
        binding.etLastCourseInstitutionName.setEnabled(status);
        binding.etLastCourseGraduationYear.setEnabled(status);
    }
}
