package br.edu.infnet.raphaelbgr.cvapp.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import br.edu.infnet.raphaelbgr.cvapp.R;
import br.edu.infnet.raphaelbgr.cvapp.base.BaseFragment;
import br.edu.infnet.raphaelbgr.cvapp.databinding.FragmentPublicationsBinding;

public class PublicationsFragment extends BaseFragment {

    public static final String TAG = PublicationsFragment.class.getSimpleName();

    private FragmentPublicationsBinding binding = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentPublicationsBinding.bind(inflater.inflate(R.layout.fragment_publications, container, false));
        setRetainInstance(true);
        setFieldsStatus(false);
        activityCallback.setTitle(getString(R.string.publications));
        viewForComparison = binding.etLastPublication;
        activityCallback.setFabVisibility(true);
        onResumeDrawerSelection = R.id.publications;
        return binding.getRoot();
    }

    public void populateFields() {
        binding.etLastPublication.setText(singleEntry.getLastPublicationName());
        binding.etDescription.setText(singleEntry.getLastPublicationDescription());
        binding.etPublicationYear.setText(singleEntry.getLastPublicationYear());
    }

    protected void readFields() {
        singleEntry.setLastPublicationName(binding.etLastPublication.getText().toString());
        singleEntry.setLastPublicationDescription(binding.etDescription.getText().toString());
        singleEntry.setLastPublicationYear(binding.etPublicationYear.getText().toString());
    }

    protected void setFieldsStatus(boolean status) {
        binding.etLastPublication.setEnabled(status);
        binding.etDescription.setEnabled(status);
        binding.etPublicationYear.setEnabled(status);
    }
}
