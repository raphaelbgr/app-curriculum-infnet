package br.edu.infnet.raphaelbgr.cvapp.data.tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import br.edu.infnet.raphaelbgr.cvapp.App;
import br.edu.infnet.raphaelbgr.cvapp.data.SharedPrefsDao;


public class TaskAsyncGetFromDisk extends AsyncTask<String, Void, Object> {

    private final String TAG = TaskAsyncGetFromDisk.this.getClass().getSimpleName();

    private SharedPreferences sharedPref;
    private GetFromDiskCallBack callBack;

    public TaskAsyncGetFromDisk(GetFromDiskCallBack callBack) {
        this.callBack = callBack;
    }

    public interface GetFromDiskCallBack {
        void getFromDiskCallBack(Object object);
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        callBack.getFromDiskCallBack(o);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        sharedPref = App.getInstance().getSharedPreferences(SharedPrefsDao.TAG, Context.MODE_PRIVATE);
    }

    @Override
    protected Object doInBackground(String... strings) {
        Object object = null;
        try {
            String key = strings[0];
            Class<?> aClass = Class.forName("br.edu.infnet.raphaelbgr.cvapp.data.datamodel." + strings[1]);
            String json = sharedPref.getString(key, null);
            object = new Gson().fromJson(json, aClass);
        } catch (JsonSyntaxException | ClassNotFoundException e) {
            Log.w(TAG, e.getCause());
        }
        return object;
    }
}
