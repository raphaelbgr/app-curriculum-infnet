package br.edu.infnet.raphaelbgr.cvapp.navigator;


import android.app.Activity;
import android.content.Intent;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import br.edu.infnet.raphaelbgr.cvapp.activities.CurriculumActivity;
import br.edu.infnet.raphaelbgr.cvapp.fragments.AbilitiesFragment;
import br.edu.infnet.raphaelbgr.cvapp.fragments.AcademicFragment;
import br.edu.infnet.raphaelbgr.cvapp.fragments.CoursesFragment;
import br.edu.infnet.raphaelbgr.cvapp.fragments.PersonalInfoFragment;
import br.edu.infnet.raphaelbgr.cvapp.fragments.PreviewFragment;
import br.edu.infnet.raphaelbgr.cvapp.fragments.ProfessionalExperienceFragment;
import br.edu.infnet.raphaelbgr.cvapp.fragments.PublicationsFragment;

public class AppNavigator {

    public static void navigateToCurriculumActivity(Activity activity) {
        Intent intent = new Intent(activity, CurriculumActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    public static Fragment createPersonalInfoFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(PersonalInfoFragment.TAG);
        if (fragment == null) {
            fragment = new PersonalInfoFragment();
        }
        return fragment;
    }

    public static Fragment createProfessionalExperienceFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(ProfessionalExperienceFragment.class.getSimpleName());
        if (fragment == null) {
            fragment = new ProfessionalExperienceFragment();
        }
        return fragment;
    }

    public static Fragment createAcademicFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(AcademicFragment.TAG);
        if (fragment == null) {
            fragment = new AcademicFragment();
        }
        return fragment;
    }

    public static Fragment createCoursesFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(CoursesFragment.TAG);
        if (fragment == null) {
            fragment = new CoursesFragment();
        }
        return fragment;
    }

    public static Fragment createPublicationsFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(PublicationsFragment.TAG);
        if (fragment == null) {
            fragment = new PublicationsFragment();
        }
        return fragment;
    }

    public static Fragment createAbilitiesFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(AbilitiesFragment.TAG);
        if (fragment == null) {
            fragment = new AbilitiesFragment();
        }
        return fragment;
    }

    public static Fragment createPreviewFragment(FragmentManager manager) {
        Fragment fragment = manager.findFragmentByTag(PreviewFragment.TAG);
        if (fragment == null) {
            fragment = new PreviewFragment();
        }
        return fragment;
    }
}
