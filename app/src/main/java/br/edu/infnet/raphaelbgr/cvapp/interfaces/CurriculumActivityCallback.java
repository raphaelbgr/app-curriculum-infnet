package br.edu.infnet.raphaelbgr.cvapp.interfaces;


import android.view.View;

public interface CurriculumActivityCallback {
    void setSelectedDrawerTab(int id);

    void setFabVisibility(boolean visibility);

    void showSnackMessage(String s);

    void setTitle(String title);

    interface FabInteractor {
        void onFabEditClicked(View fab);
    }
}
