package br.edu.infnet.raphaelbgr.cvapp.base;


import static br.edu.infnet.raphaelbgr.cvapp.firebase.analytics.FirebaseAnalytics.SCREEN;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import br.edu.infnet.raphaelbgr.cvapp.App;
import br.edu.infnet.raphaelbgr.cvapp.R;
import br.edu.infnet.raphaelbgr.cvapp.data.datamodel.SingleEntry;
import br.edu.infnet.raphaelbgr.cvapp.firebase.analytics.FirebaseAnalytics;
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback;

public abstract class BaseFragment extends Fragment implements CurriculumActivityCallback.FabInteractor {

    public CurriculumActivityCallback activityCallback;

    protected SingleEntry singleEntry;

    protected View viewForComparison;

    protected @IdRes
    int onResumeDrawerSelection = 0;

    @Override
    public void onStart() {
        super.onStart();
        App.getDao().getObject(SingleEntry.class, object -> {
            singleEntry = (SingleEntry) object;
            if (singleEntry != null) {
                populateFields();
            } else singleEntry = new SingleEntry();
        });
    }

    @SuppressLint("ResourceType")
    @Override
    public void onResume() {
        super.onResume();
        FirebaseAnalytics.logAnalyticsClickEvent(getActivity(), SCREEN, getClass().getSimpleName());
        if (onResumeDrawerSelection != 0)
            activityCallback.setSelectedDrawerTab(onResumeDrawerSelection);
    }

    protected void populateFields() {
        // To be overridden.
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activityCallback = (CurriculumActivityCallback) context;
    }

    @Override
    public void onFabEditClicked(View fab) {
        if (!viewForComparison.isEnabled()) {
            fab.setAlpha(0.4f);
            setFieldsStatus(true);
        } else {
            fab.setAlpha(1f);
            setFieldsStatus(false);
            readFields();
            App.getDao().saveObject(new Gson().toJson(singleEntry));
            activityCallback.showSnackMessage(getString(R.string.data_saved));
        }
    }

    protected abstract void setFieldsStatus(boolean status);

    protected abstract void readFields();
}
