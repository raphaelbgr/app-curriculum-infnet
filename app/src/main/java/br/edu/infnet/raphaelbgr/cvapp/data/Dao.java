package br.edu.infnet.raphaelbgr.cvapp.data;


import br.edu.infnet.raphaelbgr.cvapp.data.tasks.TaskAsyncGetFromDisk;

public interface Dao {

    void saveObject(Object object);

    void getObject(String key, Class<?> aClass, TaskAsyncGetFromDisk.GetFromDiskCallBack callBack);

    void getObject(Class<?> aClass, TaskAsyncGetFromDisk.GetFromDiskCallBack callBack);

    void eraseAllData();
}
