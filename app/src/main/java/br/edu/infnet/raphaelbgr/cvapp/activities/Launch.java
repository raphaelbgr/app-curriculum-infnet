package br.edu.infnet.raphaelbgr.cvapp.activities;

import android.os.Bundle;

import br.edu.infnet.raphaelbgr.cvapp.navigator.AppNavigator;
import br.edu.infnet.raphaelbgr.cvapp.base.BaseActivity;

public class Launch extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppNavigator.navigateToCurriculumActivity(this);
    }
}
