package br.edu.infnet.raphaelbgr.cvapp.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import br.edu.infnet.raphaelbgr.cvapp.R;
import br.edu.infnet.raphaelbgr.cvapp.base.BaseFragment;
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalInfoFragment extends BaseFragment implements CurriculumActivityCallback.FabInteractor {

    public static final String TAG = PersonalInfoFragment.class.getSimpleName();

    @BindView(R.id.name_edit_text)
    EditText nameEditText;
    @BindView(R.id.text_input_name)
    TextInputLayout textInputName;
    @BindView(R.id.address_edit_text)
    EditText addressEditText;
    @BindView(R.id.text_input_address)
    TextInputLayout textInputAddress;
    @BindView(R.id.state_edit_text)
    EditText stateEditText;
    @BindView(R.id.text_input_state)
    TextInputLayout textInputState;
    @BindView(R.id.city_edit_text)
    EditText cityEditText;
    @BindView(R.id.text_input_city)
    TextInputLayout textInputCity;
    @BindView(R.id.phone_edit_text)
    EditText phoneEditText;
    @BindView(R.id.text_input_phone)
    TextInputLayout textInputPhone;
    @BindView(R.id.email_edit_text)
    EditText emailEditText;
    @BindView(R.id.text_input_email)
    TextInputLayout textInputEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_info, container, false);
        ButterKnife.bind(this, view);
        setRetainInstance(true);
        setFieldsStatus(false);
        activityCallback.setTitle(getString(R.string.personal_info));
        activityCallback.setFabVisibility(true);
        onResumeDrawerSelection = R.id.personal_info;
        viewForComparison = nameEditText;
        return view;
    }

    protected void populateFields() {
        nameEditText.setText(singleEntry.getName());
        addressEditText.setText(singleEntry.getAddress());
        stateEditText.setText(singleEntry.getState());
        cityEditText.setText(singleEntry.getCity());
        phoneEditText.setText(singleEntry.getPhone());
        emailEditText.setText(singleEntry.getEmail());
    }

    protected void readFields() {
        singleEntry.setName(nameEditText.getText().toString());
        singleEntry.setAddress(addressEditText.getText().toString());
        singleEntry.setState(stateEditText.getText().toString());
        singleEntry.setCity(cityEditText.getText().toString());
        singleEntry.setPhone(phoneEditText.getText().toString());
        singleEntry.setEmail(emailEditText.getText().toString());
    }

    protected void setFieldsStatus(boolean status) {
        nameEditText.setEnabled(status);
        addressEditText.setEnabled(status);
        stateEditText.setEnabled(status);
        cityEditText.setEnabled(status);
        phoneEditText.setEnabled(status);
        emailEditText.setEnabled(status);
        nameEditText.setEnabled(status);
    }
}
