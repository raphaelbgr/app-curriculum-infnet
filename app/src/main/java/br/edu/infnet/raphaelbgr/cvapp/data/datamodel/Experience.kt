package br.edu.infnet.raphaelbgr.cvapp.data.datamodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
public class Experience : ResumeEvent(), Parcelable