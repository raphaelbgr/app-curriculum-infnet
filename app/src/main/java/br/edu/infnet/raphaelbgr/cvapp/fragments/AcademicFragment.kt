package br.edu.infnet.raphaelbgr.cvapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.edu.infnet.raphaelbgr.cvapp.R
import br.edu.infnet.raphaelbgr.cvapp.adapter.ExperienceAdapter
import br.edu.infnet.raphaelbgr.cvapp.base.BaseFragment
import br.edu.infnet.raphaelbgr.cvapp.data.datamodel.ResumeEvent
import br.edu.infnet.raphaelbgr.cvapp.databinding.FragmentAcademicBinding
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback.FabInteractor

class AcademicFragment : BaseFragment(), FabInteractor {
    private var binding: FragmentAcademicBinding? = null
    private var adapter: ExperienceAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAcademicBinding.bind(
            inflater.inflate(
                R.layout.fragment_academic,
                container,
                false
            )
        )
        retainInstance = true
        setFieldsStatus(false)
        activityCallback.setTitle(getString(R.string.formation))
        //        viewForComparison = binding.etHighestDegree;
        onResumeDrawerSelection = R.id.academic
        activityCallback.setFabVisibility(true)
        return binding!!.root
    }

    override fun populateFields() {
        adapter = ExperienceAdapter()
        singleEntry?.school?.let {
            adapter!!.setResumeEventList(singleEntry.school as MutableList<ResumeEvent>)
        }
    }

    override fun readFields() {
//        singleEntry.setHighestDegree(binding.etHighestDegree.getText().toString());
//        singleEntry.setInstitutionName(binding.etInstitutionName.getText().toString());
//        singleEntry.setGraduationYear(binding.etGraduationYear.getText().toString());
    }

    override fun setFieldsStatus(status: Boolean) {
//        binding.etHighestDegree.setEnabled(status);
//        binding.etInstitutionName.setEnabled(status);
//        binding.etGraduationYear.setEnabled(status);
    }

    companion object {
        @JvmField
        val TAG = AcademicFragment::class.java.simpleName
    }
}