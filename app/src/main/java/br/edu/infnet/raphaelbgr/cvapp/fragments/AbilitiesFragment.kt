package br.edu.infnet.raphaelbgr.cvapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.edu.infnet.raphaelbgr.cvapp.R
import br.edu.infnet.raphaelbgr.cvapp.base.BaseFragment
import br.edu.infnet.raphaelbgr.cvapp.databinding.FragmentAbilitiesBinding
import br.edu.infnet.raphaelbgr.cvapp.interfaces.CurriculumActivityCallback.FabInteractor

class AbilitiesFragment : BaseFragment(), FabInteractor {
    private var _binding: FragmentAbilitiesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentAbilitiesBinding.bind(
                inflater.inflate(R.layout.fragment_abilities, container, false)
        )
        onResumeDrawerSelection = R.id.abilities
        activityCallback.setTitle(getString(R.string.abilities))
        retainInstance = true
        viewForComparison = binding.etAbilities
        activityCallback.setFabVisibility(true)
        setFieldsStatus(false)
        return binding.root
    }

    override fun populateFields() {
        binding.etAbilities.setText(singleEntry!!.abilities)
        binding.etLanguages.setText(singleEntry!!.languages)
    }

    override fun readFields() {
        singleEntry.abilities = binding.etAbilities.text.toString()
        singleEntry.languages = binding.etLanguages.text.toString()
    }

    override fun setFieldsStatus(status: Boolean) {
        binding.etAbilities.isEnabled = status
        binding.etLanguages.isEnabled = status
    }

    companion object {
        @JvmField
        val TAG = AbilitiesFragment::class.java.simpleName
    }
}