package br.edu.infnet.raphaelbgr.cvapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import br.edu.infnet.raphaelbgr.cvapp.data.datamodel.ResumeEvent
import br.edu.infnet.raphaelbgr.cvapp.data.datamodel.School
import br.edu.infnet.raphaelbgr.cvapp.databinding.ItemAcademicExperienceBinding

class ExperienceAdapter : RecyclerView.Adapter<ExperienceAdapter.BindableHolder>() {

    var viewListModeSelected = -1
    val viewListModeAcademic = -179
    val viewTypeBtn = -17
    var viewList: MutableList<Any> = mutableListOf()

    fun setResumeEventList(list: MutableList<ResumeEvent>?) {
        viewList.clear()
        list?.let {
            viewList.addAll(it)
            if (list.isNotEmpty() && list.first() is School) {
                viewListModeSelected = viewListModeAcademic
            }
        }
        viewList.add(viewTypeBtn)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableHolder {
        val itemBinding = ItemAcademicExperienceBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return AcademicHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BindableHolder, position: Int) {
        viewList.let {
            holder.bind(it[position])
        }
    }

    override fun getItemCount(): Int {
        return viewList.size ?: 0
    }


    abstract class BindableHolder(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(obj: Any)
    }

    class AcademicHolder(private val binding: ItemAcademicExperienceBinding) :
        BindableHolder(binding) {
        override fun bind(obj: Any) {
            if (obj is School) {
                binding.etHighestDegree.setText(obj.courseName)
                binding.etInstitutionName.setText(obj.institutionName)
                binding.etGraduationYear.setText(obj.graduationYear)
            }
        }
    }
}
