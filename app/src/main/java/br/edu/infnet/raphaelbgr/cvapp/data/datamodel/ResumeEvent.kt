package br.edu.infnet.raphaelbgr.cvapp.data.datamodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by ibrahim on 1/18/18.
 */
@Parcelize
open class ResumeEvent(
        var institutionName: String? = null,
        var courseName: String? = null,
        val subtitle: String? = null,
        var description: String? = null,
        val fromDate: Date? = null,
        val toDate: Date? = null,
        var graduationYear: String? = null
) : Parcelable