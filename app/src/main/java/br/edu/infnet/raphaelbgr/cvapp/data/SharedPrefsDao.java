package br.edu.infnet.raphaelbgr.cvapp.data;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import br.edu.infnet.raphaelbgr.cvapp.data.tasks.TaskAsyncGetFromDisk;

public class SharedPrefsDao implements Dao {

    public static final String TAG = SharedPrefsDao.class.getSimpleName();
    private static final String USER_DATA_KEY = "saved_user_data";
    private final Application application;

    private SharedPrefsDao(Application application) {
        this.application = application;
    }

    @Override
    public void saveObject(Object object) {
        new TaskAsyncSaveToDisk(application.getSharedPreferences(TAG, Context.MODE_PRIVATE))
                .execute(USER_DATA_KEY, object.toString());
    }

    @Override
    public void getObject(String key, Class<?> aClass, TaskAsyncGetFromDisk.GetFromDiskCallBack callBack) {
        new TaskAsyncGetFromDisk(callBack).execute(key, aClass.getSimpleName());
    }

    @Override
    public void getObject(Class<?> aClass, TaskAsyncGetFromDisk.GetFromDiskCallBack callBack) {
        new TaskAsyncGetFromDisk(callBack).execute(USER_DATA_KEY, aClass.getSimpleName());
    }

    private static class TaskAsyncSaveToDisk extends AsyncTask<String, Void, Void> {
        private final SharedPreferences sharedPreferences;

        public TaskAsyncSaveToDisk(SharedPreferences sharedPreferences) {
            this.sharedPreferences = sharedPreferences;
        }

        @Override
        protected Void doInBackground(String... strings) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(strings[0], strings[1]);
            editor.apply();
            return null;
        }
    }

    @Override
    public void eraseAllData() {
        SharedPreferences sharedPref = application.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

    public static final class Builder {
        public static SharedPrefsDao init(Application application) {
            return new SharedPrefsDao(application);
        }
    }
}
