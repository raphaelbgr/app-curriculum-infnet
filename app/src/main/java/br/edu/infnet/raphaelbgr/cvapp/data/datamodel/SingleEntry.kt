package br.edu.infnet.raphaelbgr.cvapp.data.datamodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class SingleEntry(
    var name: String? = null,
    var address: String? = null,
    var state: String? = null,
    var city: String? = null,
    var phone: String? = null,
    var email: String? = null,
    var abilities: String? = null,
    var languages: String? = null,
    var enterpriseName: String? = null,
    var lastSalary: String? = null,
    var lastJobTitle: String? = null,
    var lastPublicationName: String? = null,
    var lastPublicationDescription: String? = null,
    var lastPublicationYear: String? = null,
    var school: MutableList<School>? = null,
    var lastCourseName: String? = null,
    var lastCourseInstitutionName: String? = null,
    var lastCourseGraduationYear: String? = null
) : Parcelable {

    fun getPublicationsAsList(): MutableList<Publication> {
        val publication = Publication()
        publication.institutionName = lastPublicationName
        publication.description = lastPublicationDescription
        publication.graduationYear = lastPublicationYear
        val list = mutableListOf<Publication>()
        list.add(publication)
        return list
    }

    fun getExperienceAsList(): MutableList<Experience> {
        val experience = Experience()
        experience.institutionName = enterpriseName
        experience.description = lastJobTitle
        experience.graduationYear = lastPublicationYear
        val list = mutableListOf<Experience>()
        list.add(experience)
        return list
    }
}