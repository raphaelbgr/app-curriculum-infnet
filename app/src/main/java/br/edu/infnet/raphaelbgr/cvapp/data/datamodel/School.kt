package br.edu.infnet.raphaelbgr.cvapp.data.datamodel

import kotlinx.android.parcel.Parcelize

@Parcelize
class School : ResumeEvent()